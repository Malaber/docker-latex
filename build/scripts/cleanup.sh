#!/usr/bin/env bash

sudo -u aur yay --afterclean --removemake --save
pacman -Qtdq | xargs -r pacman --noconfirm -Rcns
pacman -R --noconfirm `cat /build/packages/pacman-temp /build/packages/yay-temp`
rm -rf /home/aur/.cache
rm -rf /build
rm -rf /var/cache/pacman
