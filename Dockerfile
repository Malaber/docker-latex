FROM archlinux:latest

COPY build /build

RUN /build/scripts/install_packages.sh && \
    /build/scripts/install_yay.sh && \
    sudo -u aur yay -S --noconfirm `cat /build/packages/yay /build/packages/yay-temp` && \
    sudo -u aur sh -c 'yes | tllocalmgr install `cat /build/packages/tllocalmgr`' && texhash && \
    /build/scripts/set_german_time.sh && \
    /build/scripts/cleanup.sh

VOLUME ["/latex"]
WORKDIR /latex
CMD ["make","remake"]
